import Song from "../models/songs.js";
import mongoose from "mongoose";
export const addSong = async (req, res) => {
  const song = req.body;
  const newSong = new Song({
    ...song,
  });

  try {
    await newSong.save();
    res.status(201).json(newSong);
  } catch (error) {
    res.status(409).json({ message: error.message, song });
  }
};

export const getSongs = async (req, res) => {
  try {
    const songs = await Song.find();
    res.status(200).json({ data: songs });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const getSong = async (req, res) => {
  const { id } = req.params;
  try {
    const song = await Song.findById(id);
    res.status(200).json({ data: song });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const editSong = async (req, res) => {
  const { id: _id } = req.params;
  const song = req.body;

  try {
    if (!mongoose.Types.ObjectId.isValid(_id))
      return res.status(404).send("No song with that id");

    const updatedSong = await Song.findByIdAndUpdate(
      _id,
      { ...song, _id },
      {
        new: true,
      }
    );
    res.json(updatedSong);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};
export const rateSong = async (req, res) => {
  const { id } = req.params;
  const rate = req.body;
  // const stringId = JSON.stringify(userId);

  if (rate.user === null) return res.json({ message: "Unauthenticated" });

  // if (!req.userId) return res.json({ message: "Unauthenticated" });

  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send("No song with that id");

  const song = await Song.findById(id);
  // console.log(userId);

  // const index = song.rating.findIndex((songId) => id === String(userId));
  const ratingUser = song.rating.map((r) => r.split(":")[0]);

  const userIndex = ratingUser.findIndex(
    (songId) => songId === String(rate.user)
  );

  console.log(userIndex);
  if (userIndex === -1) {
    song.rating.push(`${rate.user}:${rate.rating}`);
  } else if (rate.rating === null) {
    song.rating = song.rating.filter(
      (r) => r.split(":")[0] !== String(rate.user)
    );
  } else song.rating.splice(userIndex, 1, `${rate.user}:${rate.rating}`);
  const updatedSong = await Song.findByIdAndUpdate(id, song, {
    new: true,
  });
  res.json(updatedSong);
};

export const deleteSong = async (req, res) => {
  const { id: _id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(_id))
    return res.status(404).json("No song with that id.");
  await Song.findByIdAndRemove(_id);
  console.log("DELETE!");
  res.json({ message: "Song deleted successfully." });
};

export const searchSong = async (req, res) => {
  const { searchQuery } = req.params;
  console.log(req);

  try {
    const title = new RegExp(searchQuery, "i");
    const lyrics = new RegExp(searchQuery, "i");
    //find in lyrics and title
    // const songs = await Song.find({ $or: [{ title }, { lyrics }] });
    //find in title only
    const songs = await Song.find({ title });
    if (!songs.length) {
      const songs = await Song.find({ lyrics });
      return res.json({ data: songs });
    } else {
      res.json({ data: songs });
    }
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const addToFavorites = async (req, res) => {
  const { id } = req.params;
  const user = req.body;
  console.log(req.body);

  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send("No song with that id");

  const song = await Song.findById(id);

  const index = song.favorites.findIndex((id) => id === String(user.user));

  console.log(index);

  if (index === -1) {
    song.favorites.push(user.user);
  } else {
    song.favorites = song.favorites.filter((id) => id !== String(user.user));
  }
  const updatedSong = await Song.findByIdAndUpdate(id, song, {
    new: true,
  });

  console.log(updatedSong);

  res.json(updatedSong);
};

export const commentSong = async (req, res) => {
  const { id } = req.params;
  const value = req.body;
  console.log(value);
  console.log(id);

  const song = await Song.findById(id);

  song.comments.push(value.comment);

  const updatedSong = await Song.findByIdAndUpdate(id, song, { new: true });
  res.json(updatedSong);
};

export const getLimitedSongs = async (req, res) => {
  try {
    const LIMIT = 8;
    const limitedSongs = await Song.find().sort({ _id: -1 }).limit(LIMIT);

    res.status(200).json({ data: limitedSongs });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};
