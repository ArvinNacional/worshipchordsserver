import express from "express";

import {
  addSong,
  getSongs,
  getSong,
  editSong,
  deleteSong,
  searchSong,
  rateSong,
  addToFavorites,
  commentSong,
  getLimitedSongs,
} from "../controllers/songs.js";

import { userAuth } from "../middleware/auth.js";

const router = express.Router();
//home songs
router.get("/home", getLimitedSongs);
// addsong
router.post("/add", addSong);
// retrieve songs
router.get("/", getSongs);
// get specific song
router.get("/:id", getSong);
// edit song
router.patch("/:id", editSong);
// delete song
router.delete("/:id", deleteSong);
// search Song
router.get("/search/:searchQuery", searchSong);
// rate song
router.patch("/:id/rateSong", rateSong);
// addToFavorites
router.patch("/:id/favorite", addToFavorites);
// comment
router.post(`/:id/commentSong`, commentSong);

export default router;
