import mongoose from "mongoose";

const songSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  writer: {
    type: String,
    required: true,
  },
  originalKey: {
    type: String,
    required: true,
  },
  lyrics: {
    type: String,
    required: true,
  },
  imageLink: String,
  tags: [String],
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  rating: {
    type: [String],
    default: [],
  },
  favorites: [String],
  comments: {
    type: [String],
    default: [],
  },
  videoId: {
    type: String,
    required: true,
  },
});

const Song = mongoose.model("Song", songSchema);

export default Song;
