import jwt from "jsonwebtoken";

export const userAuth = async (req, res, next) => {
  try {
    let token = req.headers.authorization.split(" ")[1];
    let isCustomAuth = token.length < 500;

    let decodedData;

    if (token && isCustomAuth) {
      decodedData = jwt.verify(token, "test");
      req.userId = decodedData.id;
    } else {
      decodedData = jwt.decode(token);
      req.userId = decodedData.sub;
    }

    next();
  } catch (error) {
    console.log(error);
  }
};

export const adminAuth = async (req, res) => {
  let token = req.headers.authorization;
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (err, data) => {
      return err ? res.send({ auth: "failed" }) : next();
    });
  }
};
